import angular from 'angular';
import JSZip from 'jszip';
import FileSaver from 'file-saver';

import convertYfullToFinancier from './convertYfullToFinancier';

let dragDrop = (convertYfullToFinancier, $rootScope, $q) => {
  return {
    restrict: 'E',
    transclude: true,
    scope: true,
    template: '<input accept=".ynab4,.yfull" type="file" id="uploadfile" /> <ng-transclude></ng-transclude>',
    link: (scope, element, attrs) => {
      const upload = element.find('input');

      upload.bind('change', () => {
        $rootScope.success = false;
        $rootScope.error = false;

        const budgetName = upload[0].files[0].name.split('~')[0];

        let promise;

        if (upload[0].files[0].name === 'Budget.yfull') {
          // If windows we can't upload the .ynab4 directory
          promise = parseFile(upload[0].files[0]);
        } else {
          promise = JSZip.loadAsync(upload[0].files[0])
          .then(function(zip) {
            let f;
            Object.keys(zip.files).sort().forEach(fileName => {

              if (fileName.indexOf('/Budget.yfull') !== -1) {
                f = fileName;
              }
            });

            return zip.files[f]
            .async('string');
          });
        }

        $q.when(promise)
        .then(contents => {
          return JSON.parse(contents);
        })
        .then(yfull => {
          return convertYfullToFinancier(budgetName, yfull);
        })
        .then(financier => {
          const blob = new Blob([JSON.stringify(financier, null, 2)], {
            type: 'text/plain;charset=utf-8'
          });

          FileSaver.saveAs(blob, `${budgetName}-migration.json`);
          $rootScope.success = true;
        })
        .catch(e => {
          $rootScope.error = true;
        });
      });

      angular.element(document.body).on('click', () => {
        upload[0].click();
      });

      scope.reset = () => {
        scope.success = false;
        scope.error = false;
      }

      function parseFile(file) {
        var reader = new FileReader();

        // Read in the image file as a data URL.
        reader.readAsText(file);

        return $q((resolve, reject) => {
          // Closure to capture the file information.
          reader.onload = (() => {
            return e => {
              resolve(e.target.result);
            };
          })(file);
        });
      }
    }
  }
};

dragDrop.$inject = ['convertYfullToFinancier', '$rootScope', '$q'];

angular.module('dragDrop', [convertYfullToFinancier])
  .directive('dragDrop', dragDrop);