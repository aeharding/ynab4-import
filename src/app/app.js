import angular from 'angular';

import '../style/app.scss';

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};

class AppCtrl {
  constructor() {
    this.url = 'https://github.com/preboot/angular-webpack';
  }
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, ['cloudComputing', 'dragDrop'])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl);

import cloudComputing from './cloudComputing';
import dragDrop from './dragDrop';

export default MODULE_NAME;